This is small ATM prototype to test React hooks.

To run this project execute: (assuming node is installed).
```
npm install
npm start
```

To run tests:
```
npm run test
```

This project is bootstraped with CRA, so it uses by default React for UI and Jest for testing. 

The flow should be like this:
* select language
* enter 4 letter pin
* enter amount to withdraw
* grab money
* exit screen

Code should be easy to follow as it super small. All screens are in `/screens`, tests are ending with `.spec.js`