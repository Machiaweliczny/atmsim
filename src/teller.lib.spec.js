import { testHook } from './tests/hooks'
import { useTeller } from "./teller.lib";

describe("teller", () => {
  describe("starting state", () => {
    it("should start with english language", () => {
      const h = testHook(useTeller);
      expect(h.state.lang).toEqual("en");
    });
    it("should start with welcome screen", () => {
      const h = testHook(useTeller);
      expect(h.state.stage).toEqual("welcome");
    });
  });

  describe("selectLanguage", () => {
    it("should set selected language", () => {
      const h = testHook(useTeller);
      h.api.selectLanguage("de");
      expect(h.state.lang).toEqual("de");
    });

    it("should change screen to Pin", () => {
      const h = testHook(useTeller);
      h.api.selectLanguage("de");
      expect(h.state.stage).toEqual("pin");
    });
  });

  describe("checkPin", () => {
    it("should change screen to Withdraw", () => {
      const h = testHook(useTeller);
      h.api.checkPin("1234");
      expect(h.state.stage).toEqual("withdraw");
    });
  });

  describe("widthdraw(180)", () => {
    it("should set notes to [100, 50, 20, 10]", () => {
      const h = testHook(useTeller);
      h.api.withdraw(180);
      expect(h.state.notes).toEqual([100, 50, 20, 10]);
    });

    it("should change screen to Notes", () => {
      const h = testHook(useTeller);
      h.api.withdraw(180);
      expect(h.state.stage).toEqual("notes");
    });
  });

  describe("grabNotes()", () => {
    it("should set notes to []", () => {
      const h = testHook(useTeller);
      h.api.grabNotes();
      expect(h.state.notes.length).toEqual(0);
    });

    it("should change screen to Exit", () => {
      const h = testHook(useTeller);
      h.api.grabNotes();
      expect(h.state.stage).toEqual("exit");
    });
  });

  describe("exit()", () => {
    it("should change to Welcome screen after 3s", done => {
      const h = testHook(useTeller);
      h.api.exit();
      expect(h.state.stage).toEqual("exit");
      setTimeout(() => {
        expect(h.state.stage).toEqual("welcome");
        expect(h.state.lang).toEqual("en");
        done();
      }, 3001);
    });
  });
});
