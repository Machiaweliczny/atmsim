import React from 'react';
import './Keyboard.css';

function defaultDisplayAmount(amount){
  return amount;
}

export function Keyboard({ onSubmit, onExit, displayAmount = defaultDisplayAmount }){
    const [amount, setAmount] = React.useState('');
    const addDigit = (d) => () => setAmount(amount + d)
    const clear = () => setAmount('');
    const handleSubmit = (e) => onSubmit(parseInt(amount, 10) || 0);
    const toButton = (d) =><div className="keyboard__button-cell"><button onClick={addDigit(d)} > {d} </button></div>
    
    return (<section className="keyboard">
      <div className="keyboard-display"> {" "} { displayAmount(amount) }  {" "}</div>
      <section className="keyboard-input">
      <div className="keyboard-col">
        {[1,4,7].map(toButton)}
      </div>
      <div className="keyboard-col">
      {[2,5,8, 0].map(toButton)}
      </div>
      <div className="keyboard-col">
        {[3,6,9].map(toButton)}
      </div>
      <div className="keyboard-col">
      <button onClick={handleSubmit}>SUBMIT</button>
        <button onClick={clear}>CLEAR</button>
        <button onClick={() => onExit()}> EXIT</button>
      </div>
    </section>  
    </section>)
  }