export const translations = {
  en: {
    teller: 'ATM',
    currency: '$',
    welcome: "Welcome",
    actions: {
      withdraw: "Widthdraw"
    },
    screens: {
      exit: {
        bye: 'Bye!'
      }
    }
  },
  pl: {
    teller: 'Bankomat',
    currency: "zł",
    welcome: "Witaj",
    actions: {
      withdraw: "Wypłata"
    },
    screens: {
      exit: {
        bye: 'Do zobaczenia!'
      }
    }
  },
  de: {
    teller: 'ATM',
    currency: '€',
    welcome: "herzlich willkommen",
    actions: {
      withdraw: "abheben"
    },
    screens: {
      exit: {
        bye: 'bis später'
      }
    }
  }
};
