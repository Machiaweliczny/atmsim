import React from "react";
import "./notes.css";

export function Notes({ notes , t, next }) {
  return (
    <section className="notes__container">
    <section className="notes" onClick={next}>
      {notes.map((value, index) => (
        <div className="note" style={{animationDelay: `${index*0.1}s`}}> {value} {t.currency} </div>
      ))}
    </section>
    </section>
  );
}
