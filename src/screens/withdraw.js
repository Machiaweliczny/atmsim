import React from "react";
import { Keyboard } from "../components/Keyboard";

export function Withdraw({ next, exit }) {
  const handleSubmit = next;
  const handleExit = exit;
  return (
    <div className="App">
      <h2> Withdraw </h2>
      <h3> Please provide amount:</h3>
      <Keyboard onSubmit={handleSubmit} onExit={handleExit} />
    </div>
  );
}
