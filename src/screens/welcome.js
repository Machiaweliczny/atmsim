import React from "react";

export function Welcome({ next, exit }) {
  return (
    <div className="App">
      <section className="main">
        <div className="main-panel main-left">
          <button onClick={() => next("en")}> English </button>
          <button onClick={() => next("de")}> Deutsch </button>
        </div>
        <div className="main-panel main-right">
          <button onClick={() => next("pl")}> Polski </button>
          <button onClick={() => exit()}> Exit </button>
        </div>
      </section>
    </div>
  );
}
