import React from 'react';
import { Keyboard } from "../components/Keyboard";

function displayPin(pin){
    return Array.from(pin).map(c => '*').join('');
}


export function Pin({ next , exit }){
    const [error, setError] = React.useState('');

    const handleSubmit = (value) => {
        const number = parseInt(value, 10);
        if(number >= 999 && number <= 10000){
            next();
         } else {
          // TODO: switch to T
          setError('Provided PIN is invalid.');
        }
    }
    const handleExit = exit;
    
    return (<section class="App">
        <h2> Please provide PIN: </h2>
        <Keyboard onSubmit={handleSubmit} onExit={handleExit} displayAmount={displayPin} />
        <div style={errorStyle}>{error}</div>
    </section>);
}

const errorStyle = {
    marginTop: "30px",
    color: "red"
}