import React from "react";

export function Exit({ t }) {
  const c = useCountdown(3);
  return (
    <div className="App">
      <section className="main main-exit">
        <h1>{t.screens.exit.bye}</h1>
      </section>
      <div>{c}...</div>
    </div>
  );
}

function useCountdown(sec) {
  const [c, setC] = React.useState(sec);
  React.useEffect(() => {
    const interval = setInterval(() => {
      setC(c => (c > 0 ? c - 1 : 0));
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return c;
}
