import React from "react";
import ReactDOM from "react-dom";

import { useTeller } from './teller.lib'

import { Welcome } from "./screens/welcome";
import { Pin } from './screens/pin';
import { Withdraw } from "./screens/withdraw";
import { Notes } from "./screens/notes";
import { Exit } from "./screens/exit";

import "./styles.css";

function App() {
  const [state, api] = useTeller();

  return (
    <section className="App" >
      <h1> {state.t.teller} </h1>
      <Stages show={state.stage}>
        <Welcome t={state.t} next={api.selectLanguage} exit={api.exit} />
        <Pin t={state.t} next={api.checkPin} exit={api.exit} />
        <Withdraw t={state.t} withdraw={api.withdraw} exit={api.exit} next={api.withdraw} />
        <Notes t={state.t} notes={state.notes} next={api.grabNotes} />
        <Exit t={state.t} reset={api.reset} />
      </Stages>
    </section>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);

function Stages({ show, children }) {
  const stage = children.filter(c => c.type.name.toLowerCase() === show);
  return stage;
}
