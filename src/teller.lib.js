import React from 'react';
import { translations } from './translations';

function getNotes(amount) {
  console.log("getNotes", amount)
  amount = Math.floor(amount / 10) * 10;
  if (amount <= 0) return [];
  for (let note of [100, 50, 20, 10]) {
    if (amount >= note) return [note].concat(getNotes(amount - note));
  }
}

export function useTeller() {
  const [stage, setStage] = React.useState("welcome");
  const [lang, setLang] = React.useState("en");
  const [notes, setNotes] = React.useState([]);
  const t = translations[lang];
  const reset = () => {
    setLang('en');
    setStage('welcome');
    setNotes([]);
  }
  function selectLanguage(lang) {
    setLang(lang);
    setStage("pin");
  }
  function checkPin(_pin){
    // no validation
    setStage('withdraw');
  }
  function withdraw(amount) {
    if(amount === 0) exit();
    else {
      setNotes(getNotes(amount));
      setStage('notes');  
    }
  }
  function grabNotes(){
    setNotes([]);
    exit();
  }
  function exit() {
    setStage("exit");
    setTimeout(reset, 3000)
  }
  return [{ stage, lang, notes,  t}, { selectLanguage, checkPin, withdraw, grabNotes, exit }];
}