import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";

export function testHook(hook, config) {
  const container = document.createElement("div");
  document.body.appendChild(container);
  const h = {};
  const handler = {
    get: function(obj, prop) {
      if(typeof obj[prop] === "function"){
        return (...args) => act(() => obj[prop](...args));
      } else {
        return obj[prop];
      }
    }
  };
  const TestComponent = () => {
    const [state, api] = hook(config);
    h.state = state;
    h.api = new Proxy(api, handler);
    return null;
  };
  ReactDOM.render(<TestComponent />, container);
  return h;
}
